#include <iostream>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

int main()
{
    Mat frame;
    
     // open the default camera and check if it is open
    VideoCapture cap(0); 
    if(!cap.isOpened()) return -1;
    
    // output window
    namedWindow("Camera Output", WINDOW_AUTOSIZE);
    
    // loop forever till ESC
    while(true)
    {
        // capture a frame from the camera
        cap >> frame;
        if( frame.empty() ) {
            cout << " No captured frame." << endl;
            break;
        }
            
        // display the frame
        imshow( "Camera Output", frame );

        // test for ESC
        int c = waitKey(10);
        if(c == 27 /* ESC */)
            break;
    }
    
    return 0;
}
