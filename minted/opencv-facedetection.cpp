#include <iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

using namespace std;
using namespace cv;

String face_frontal_cascade_name =
    "haarcascade/haarcascade_frontalface_alt.xml";

CascadeClassifier face_frontal_cascade;

int main()
{
    Mat frame;
    vector<Rect> faces;
    
    // open the default camera and check if it is open
    VideoCapture cap(0);
    if(!cap.isOpened())
    return -1;
    
    // load haarcascade
    if (!face_frontal_cascade.load(face_frontal_cascade_name))
        cout << "Couldn't load haarcascade classifier" << endl;
    
    // output window
    namedWindow("Camera Output", WINDOW_AUTOSIZE);
    
    // loop forever till ESC
    while(true)
    {
        // capture a frame from the camera
        cap >> frame;
        if( frame.empty() ) {
            cout << " No captured frame." << endl;
            break;
        }
        
        // detect faces
        face_frontal_cascade.detectMultiScale(frame, faces,
            1.3, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(20, 10));
        
        // draw ellipse around faces
        for( int i = 0; i < faces.size(); i++ )
        {
            Point center( faces[i].x + faces[i].width*0.5,
                faces[i].y + faces[i].height*0.5 );
            ellipse( frame, center,
                Size( faces[i].width*0.5, faces[i].height*0.5),
                0, 0, 360, Scalar( 0, 255, 255 ), 2, 8, 0 );
        }
        
        // display the frame
        imshow( "Camera Output", frame );
        
        // test for ESC
        int c = waitKey(10);
        if(c == 27 /* ESC */)
        break;
    }
    
    return 0;
}
